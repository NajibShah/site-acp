export * from "./Landing";
export * from "./Services";
export * from "./Blog";
export * from "./Contact";
export * from "./admin/Admin";
export * from "./projects/singleProject";
export * from "./projects/project-categories";
export * from "./admin";
