import { useContext } from "react";
import { ProjectsContext } from "../contexts";

export function useProjectsContext() {
  const { state, dispatch } = useContext(ProjectsContext);
  return { state, dispatch };
}
