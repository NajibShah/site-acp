/* eslint-disable react-hooks/exhaustive-deps */
import React, { useReducer, useEffect, useState } from "react";
import { getProjectsData } from "../functions";
import { projectsReducer } from "../functions";
import { ProjectsContext } from "../contexts";
import { Loader } from "../layout";
// import { useRequest, LoaderDiv } from "../../common";
// import { getUserInfo } from "../../auth";
// import { get } from "../../lib";
import { db } from "../config/firebase";

export const ProjectsDataProvider = ({ children }) => {
  const [newData, setNewData] = useState();
  const [state, dispatch] = useReducer(projectsReducer, {
    projectsData: [newData],
    selectedCategory: "",
    selectedProject: "",
  });
  // const [state, dispatch] = useReducer(automatonTagsReducer, {
  //   orgId: org,
  //   fetching: true,
  //   tagSetup: {},
  //   profileSetup: {
  //     customComponents: {
  //       header: {
  //         visibility: false,
  //         text: "",
  //       },
  //     },
  //     customStyles: {
  //       customColors: {},
  //     },
  //     customLogos: {},
  //   },
  // });
  // const [request, response, error] = useRequest(() => get(`org-setup/${org}/automaton-tags.json`));

  // useEffect(() => {
  //   if (Object.keys(state.tagSetup).length === 0) {
  //     request();
  //     dispatch({ type: "fetching" });
  //   }
  // }, [org]);

  useEffect(() => {
    getProjectsData(db).then((newData) => setNewData(newData));
  }, []);

  // useEffect(() => {
  //   if (newData) {
  //     dispatch({ type: "set-projects-data", projectsData: newData });
  //   }
  // }, [newData]);

  return (
    <ProjectsContext.Provider value={{ state, dispatch }}>
      {state.projectsData ? <Loader /> : children}
    </ProjectsContext.Provider>
  );
};
