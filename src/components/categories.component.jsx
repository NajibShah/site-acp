import React from "react";

//THIS PAGE EXISTS WITHOUT PURPOSE OR USE,
//KEEPING IT UNTIL SITE IS HANDED OVER
//FOR REFERENCE
export function Categories(props) {
  return (
    <section>
      <div className="row justify-content-end">
        <div className="col-md-10 mt-3">
          <section className="ftco-section">
            <div className="container-fluid px-3 px-md-0">
              <div className="row">
                {props.props.map((category, index) => (
                  <div key={index} className="col-md-12 project">
                    <div
                      className="img js-fullheight"
                      style={{
                        backgroundImage: `url(${require(`../assets/images/${category.image}`)})`,
                        height: "45rem",
                        width: "100%",
                      }}
                    >
                      <div className="text text-center">
                        <h2>
                          <a
                            href={
                              category.link
                                ? `/projects/${category.link}`
                                : null
                            }
                          >
                            {category.name}
                          </a>
                        </h2>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  );
}
