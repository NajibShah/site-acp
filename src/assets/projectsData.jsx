// export const projectsData =
//   [
//     {
//     "name":"atlantis",
//     "id": 1,
//     "title": "The Atlantis Mall",
//     "subtitle": "An Ultra Modern out-of-the-box design for a Mix-Use building",
//     "location": "F-17 Islamabad",
//     "client": "Margalla Builders and Developers",
//     "type": "Mix-Use",
//     "builtArea": "1,300,000 sft",
//     "mainImage": "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//     "gallery": [
//        "https://api.pcloud.com/getpubthumb?code=XZ7PIqXZC0S3kAnRWvQXQ2iIRpEPIY9sWePy&linkpassword=undefined&size=2048x916&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZtTIqXZaj01VGmMeqpSaRyuqRnDehb3YM2V&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZbgIqXZkYsu38Ysz6hPF8w861weQFYwbjB7&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZegIqXZ3cT8JSynhekU7d8mWeiiSYsoP9kV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZEgIqXZxrkhhY8ASNLy1UuNR0t4iYQXb6uy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZ0TIqXZdvhKm0cAUE4AU5Dk6rlP2R2LNmBV&linkpassword=undefined&size=2048x1505&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZ5PIqXZllRGcGNbt8msWRylUltHnjE9dnYX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZbPIqXZAdlBlqPCGEmEbzgfql89eYC66oGy&linkpassword=undefined&size=2048x1364&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZVCIqXZoUYzkh0DGR8bIEPQu6x2oQqkCPVX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto"

//     ]
//     },
//     {
//     "name":"sherali",
//     "id": 2,
//     "title": "Sikandar Sher Arcade",
//     "subtitle": "2 Kanal Commercial, Minimalist – Practical – Energy Efficient & Cost Efficient Design",
//     "location": "Swabi",
//     "client": "Mr. Tahir Kahn",
//     "type": "Commercial",
//     "builtArea": "31,400 sft",
//     "mainImage": "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//     "gallery": [
//        "https://api.pcloud.com/getpubthumb?code=XZ7dIqXZ2CxgJxrTBDSz4NnlhTPkApetHizy&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZDtIqXZm4SWkX7eMIzxR4fU2joMlfOeTgTk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZMtIqXZuWDf4E9xW3uY6UpOkrzQrRyCqPD7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZTtIqXZjtq5Prf3l6mvN3k2sYCQ9HgRl3my&linkpassword=undefined&size=2048x1125&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZ56IqXZ3zGPQVOqs00XT3Fua1Kc4hOIjkgk&linkpassword=undefined&size=1920x1080&crop=0&type=auto"
//     ]
//     },
//     {
//     "name":"mixuse",
//     "id": 3,
//     "title": "J-7 Emporium",
//     "subtitle": "An 18 storey highrise focusing on views and luxury",
//     "location": "B-17 Islamabad",
//     "client": "J-7 Group",
//     "type": "Mix-Use Highrise Tower",
//     "builtArea": "836,000 sft",
//     "mainImage": "https://api.pcloud.com/getpubthumb?code=XZjsGqXZQyDkKRTbw3mutsUKOPPITJiP4iLX&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//     "gallery": [
//        "https://api.pcloud.com/getpubthumb?code=XZnWGqXZH0RcaQPvOwf3Kw9pz1z3z8qxQEyV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZKsGqXZPsqnmPRWD14fp1lk8lpD6Fkk838X&linkpassword=undefined&size=2048x950&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZdsGqXZ24IMEEv30Y5r5RhddWTsVpXDqtmX&linkpassword=undefined&size=2048x1285&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZEsGqXZsCp46KLbuv7VH5bUeTVDckUuVhz7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZAsGqXZCzK8SAstfGXmM3wGBLVbXQ0d0a0X&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZisGqXZf5scy6ESdM40A6cmekQ6rRoAUNXk&linkpassword=undefined&size=1920x1080&crop=0&type=auto"
//     ]
//     },
//     {
//     "name":"jade",
//     "id": 4,
//     "title": "Jade Apartments",
//     "subtitle": "An 18 storey highrise focusing on views and luxury",
//     "location": "B-17 Islamabad",
//     "client": "Jade Group",
//     "type": "Mix-Use Highrise Tower",
//     "builtArea": "823,000 sft",
//     "mainImage": "https://api.pcloud.com/getpubthumb?code=XZaTGqXZRLndkvL9wRRjf81negYNaVSSPMyy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//     "gallery": [
//        "https://api.pcloud.com/getpubthumb?code=XZWgGqXZe3ufXVoKR1kciokBhogpqht0nIyX&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZugGqXZ43PGmX6ejkmygy5bgLflpunmdNCk&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//        "https://api.pcloud.com/getpubthumb?code=XZoTGqXZx6cesnnqGRb0m07Vyi1n80APIvEy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",

//     ]
//     }
//   ]

//       [
//          {
//             "name":"Commercial Projects"
//              "projects":  [{
//                               project
//                            },
//                {
//                project
//                }]
//       }

//OBJECT REFERENCE//
// const categories = [
//   {
//     catID: "",
//     category:"",
//     categoryImage:"",
//     subcategories: [
//       {
//         subCatID: "",
//         title: "",
//         subCategoryImage: "",
//         projects: [
//           {
//             projID: "",
//             id: "",
//             title: "",
//             subtitle:'',
//             location: '',
//             client: '',
//             type:'',
//             builtArea:'',
//             mainImage:'',
//             gallery: []
//           }
//         ]
//       }
//     ]
//   },
// ];

///
///
///
///
/// PROJECTS
///
const projects = [
  {
    projID: "atlantis",
    id: 0,
    title: "The Atlantis Mall",
    subtitle: "An Ultra Modern out-of-the-box design for a Mix-Use building",
    location: "F-17 Islamabad",
    client: "Margalla Builders and Developers",
    type: "Mix-Use",
    builtArea: "1,300,000 sft",
    video: false,
    mainImage:
      "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    gallery: [
      "https://api.pcloud.com/getpubthumb?code=XZ7PIqXZC0S3kAnRWvQXQ2iIRpEPIY9sWePy&linkpassword=undefined&size=2048x916&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZtTIqXZaj01VGmMeqpSaRyuqRnDehb3YM2V&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZbgIqXZkYsu38Ysz6hPF8w861weQFYwbjB7&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZegIqXZ3cT8JSynhekU7d8mWeiiSYsoP9kV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZEgIqXZxrkhhY8ASNLy1UuNR0t4iYQXb6uy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZ0TIqXZdvhKm0cAUE4AU5Dk6rlP2R2LNmBV&linkpassword=undefined&size=2048x1505&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZ5PIqXZllRGcGNbt8msWRylUltHnjE9dnYX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZbPIqXZAdlBlqPCGEmEbzgfql89eYC66oGy&linkpassword=undefined&size=2048x1364&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZVCIqXZoUYzkh0DGR8bIEPQu6x2oQqkCPVX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
    ],
  },
  {
    projID: "sherali",
    id: 1,
    title: "Sikandar Sher Arcade",
    subtitle:
      "2 Kanal Commercial, Minimalist – Practical – Energy Efficient & Cost Efficient Design",
    location: "Swabi",
    client: "Mr. Tahir Kahn",
    type: "Commercial",
    builtArea: "31,400 sft",
    video: false,
    mainImage:
      "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    gallery: [
      "https://api.pcloud.com/getpubthumb?code=XZ7dIqXZ2CxgJxrTBDSz4NnlhTPkApetHizy&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZDtIqXZm4SWkX7eMIzxR4fU2joMlfOeTgTk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZMtIqXZuWDf4E9xW3uY6UpOkrzQrRyCqPD7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZTtIqXZjtq5Prf3l6mvN3k2sYCQ9HgRl3my&linkpassword=undefined&size=2048x1125&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZ56IqXZ3zGPQVOqs00XT3Fua1Kc4hOIjkgk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    ],
  },
  {
    projID: "xyz",
    id: 2,
    title: "Xander York Zen",
    subtitle: "An Ultra Modern out-of-the-box design for a Mix-Use building",
    location: "F-17 Islamabad",
    client: "Margalla Builders and Developers",
    type: "Mix-Use",
    builtArea: "1,300,000 sft",
    video: true,
    mainImage:
      "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    gallery: [
      "https://api.pcloud.com/getpubthumb?code=XZ7PIqXZC0S3kAnRWvQXQ2iIRpEPIY9sWePy&linkpassword=undefined&size=2048x916&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZtTIqXZaj01VGmMeqpSaRyuqRnDehb3YM2V&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZbgIqXZkYsu38Ysz6hPF8w861weQFYwbjB7&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZegIqXZ3cT8JSynhekU7d8mWeiiSYsoP9kV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZEgIqXZxrkhhY8ASNLy1UuNR0t4iYQXb6uy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZ0TIqXZdvhKm0cAUE4AU5Dk6rlP2R2LNmBV&linkpassword=undefined&size=2048x1505&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZ5PIqXZllRGcGNbt8msWRylUltHnjE9dnYX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZbPIqXZAdlBlqPCGEmEbzgfql89eYC66oGy&linkpassword=undefined&size=2048x1364&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZVCIqXZoUYzkh0DGR8bIEPQu6x2oQqkCPVX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
      "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
    ],
  },
];

///
///
///
///
/// CATEGORIES
///
export const categories = [
  {
    catID: "cat0",
    category: "Architecture Design",
    categoryImage:
      "https://api.pcloud.com/getpubthumb?code=XZaTGqXZRLndkvL9wRRjf81negYNaVSSPMyy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
    // subCategories: [
    //   {
    //     subCatID: "subCat00",
    //     title: "Commercial Projects",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[2],
    //       projects[1],
    //       projects[2],
    //       projects[1],
    //       projects[2],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat01",
    //     title: "Residential Projects",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat02",
    //     title: "Corporate Projects",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    // ],
  },
  {
    catID: "cat1",
    category: "Design Consultancy",
    categoryImage:
      "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    // subCategories: [
    //   {
    //     subCatID: "subCat10",
    //     title: "Corporate",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat11",
    //     title: "Residential",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    // ],
  },
  {
    catID: "cat2",
    category: "Master Planning",
    categoryImage:
      "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
    // subCategories: [
    //   {
    //     subCatID: "subCat20",
    //     title: "Town planning",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat21",
    //     title: "Town planning",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    // ],
  },
  {
    catID: "cat3",
    category: "Marketing Content",
    categoryImage:
      "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
    // subCategories: [
    //   {
    //     subCatID: "subCat30",
    //     title: "Exterior and Interior",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",

    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat31",
    //     title: "VRs",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",

    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat32",
    //     title: "Print Media",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",

    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    //   {
    //     subCatID: "subCat33",
    //     title: "Misc",
    //     subCategoryImage:
    //       "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",

    //     projects: [
    //       projects[0],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //       projects[1],
    //       projects[0],
    //       projects[1],
    //     ],
    //   },
    // ],
  },
];

// export const projectsData = [
//   {
//     catID: "commercial",
//     category: "Commercial Projects",
//     projects: [
//       {
//         projID: "atlantis",
//         id: 1,
//         title: "The Atlantis Mall",
//         subtitle:
//           "An Ultra Modern out-of-the-box design for a Mix-Use building",
//         location: "F-17 Islamabad",
//         client: "Margalla Builders and Developers",
//         type: "Mix-Use",
//         builtArea: "1,300,000 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZ7PIqXZC0S3kAnRWvQXQ2iIRpEPIY9sWePy&linkpassword=undefined&size=2048x916&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZtTIqXZaj01VGmMeqpSaRyuqRnDehb3YM2V&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZbgIqXZkYsu38Ysz6hPF8w861weQFYwbjB7&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZegIqXZ3cT8JSynhekU7d8mWeiiSYsoP9kV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZEgIqXZxrkhhY8ASNLy1UuNR0t4iYQXb6uy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ0TIqXZdvhKm0cAUE4AU5Dk6rlP2R2LNmBV&linkpassword=undefined&size=2048x1505&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ5PIqXZllRGcGNbt8msWRylUltHnjE9dnYX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZbPIqXZAdlBlqPCGEmEbzgfql89eYC66oGy&linkpassword=undefined&size=2048x1364&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZVCIqXZoUYzkh0DGR8bIEPQu6x2oQqkCPVX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
//         ],
//       },
//       {
//         projID: "sherali",
//         id: 2,
//         title: "Sikandar Sher Arcade",
//         subtitle:
//           "2 Kanal Commercial, Minimalist – Practical – Energy Efficient & Cost Efficient Design",
//         location: "Swabi",
//         client: "Mr. Tahir Kahn",
//         type: "Commercial",
//         builtArea: "31,400 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZ7dIqXZ2CxgJxrTBDSz4NnlhTPkApetHizy&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZDtIqXZm4SWkX7eMIzxR4fU2joMlfOeTgTk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZMtIqXZuWDf4E9xW3uY6UpOkrzQrRyCqPD7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZTtIqXZjtq5Prf3l6mvN3k2sYCQ9HgRl3my&linkpassword=undefined&size=2048x1125&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ56IqXZ3zGPQVOqs00XT3Fua1Kc4hOIjkgk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         ],
//       },
//     ],
//   },
//   {
//     catID: "architecture",
//     category: "Architecture and Planning",
//     projects: [
//       {
//         projID: "atlantis",
//         id: 1,
//         title: "The Atlantis Mall",
//         subtitle:
//           "An Ultra Modern out-of-the-box design for a Mix-Use building",
//         location: "F-17 Islamabad",
//         client: "Margalla Builders and Developers",
//         type: "Mix-Use",
//         builtArea: "1,300,000 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=0QW7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZ7PIqXZC0S3kAnRWvQXQ2iIRpEPIY9sWePy&linkpassword=undefined&size=2048x916&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZtTIqXZaj01VGmMeqpSaRyuqRnDehb3YM2V&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZbgIqXZkYsu38Ysz6hPF8w861weQFYwbjB7&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZegIqXZ3cT8JSynhekU7d8mWeiiSYsoP9kV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZEgIqXZxrkhhY8ASNLy1UuNR0t4iYQXb6uy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ0TIqXZdvhKm0cAUE4AU5Dk6rlP2R2LNmBV&linkpassword=undefined&size=2048x1505&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ5PIqXZllRGcGNbt8msWRylUltHnjE9dnYX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZzPIqXZy3hjNHsHCoflEtuqUHWPR5BMMHIV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZbPIqXZAdlBlqPCGEmEbzgfql89eYC66oGy&linkpassword=undefined&size=2048x1364&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZVCIqXZoUYzkh0DGR8bIEPQu6x2oQqkCPVX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZFCIqXZSpS5pXIySA5JMy69jn3FuBGorBoV&linkpassword=undefined&size=1224x1080&crop=0&type=auto",
//         ],
//       },
//       {
//         projID: "sherali",
//         id: 2,
//         title: "Sikandar Sher Arcade",
//         subtitle:
//           "2 Kanal Commercial, Minimalist – Practical – Energy Efficient & Cost Efficient Design",
//         location: "Swabi",
//         client: "Mr. Tahir Kahn",
//         type: "Commercial",
//         builtArea: "31,400 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZ7dIqXZ2CxgJxrTBDSz4NnlhTPkApetHizy&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZDtIqXZm4SWkX7eMIzxR4fU2joMlfOeTgTk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZMtIqXZuWDf4E9xW3uY6UpOkrzQrRyCqPD7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZTtIqXZjtq5Prf3l6mvN3k2sYCQ9HgRl3my&linkpassword=undefined&size=2048x1125&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZItIqXZGeiQI8DKzwVhfhPrTilbbVTSIXgX&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZ56IqXZ3zGPQVOqs00XT3Fua1Kc4hOIjkgk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         ],
//       },
//     ],
//   },
//   {
//     catID: "residential",
//     category: "Residential Projects",
//     projects: [
//       {
//         projID: "mixuse",
//         id: 3,
//         title: "J-7 Emporium",
//         subtitle: "An 18 storey highrise focusing on views and luxury",
//         location: "B-17 Islamabad",
//         client: "J-7 Group",
//         type: "Mix-Use Highrise Tower",
//         builtArea: "836,000 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=XZjsGqXZQyDkKRTbw3mutsUKOPPITJiP4iLX&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZnWGqXZH0RcaQPvOwf3Kw9pz1z3z8qxQEyV&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZKsGqXZPsqnmPRWD14fp1lk8lpD6Fkk838X&linkpassword=undefined&size=2048x950&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZdsGqXZ24IMEEv30Y5r5RhddWTsVpXDqtmX&linkpassword=undefined&size=2048x1285&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZEsGqXZsCp46KLbuv7VH5bUeTVDckUuVhz7&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZAsGqXZCzK8SAstfGXmM3wGBLVbXQ0d0a0X&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZisGqXZf5scy6ESdM40A6cmekQ6rRoAUNXk&linkpassword=undefined&size=1920x1080&crop=0&type=auto",
//         ],
//       },
//       {
//         projID: "jade",
//         id: 4,
//         title: "Jade Apartments",
//         subtitle: "An 18 storey highrise focusing on views and luxury",
//         location: "B-17 Islamabad",
//         client: "Jade Group",
//         type: "Mix-Use Highrise Tower",
//         builtArea: "823,000 sft",
//         mainImage:
//           "https://api.pcloud.com/getpubthumb?code=XZaTGqXZRLndkvL9wRRjf81negYNaVSSPMyy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//         gallery: [
//           "https://api.pcloud.com/getpubthumb?code=XZWgGqXZe3ufXVoKR1kciokBhogpqht0nIyX&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZugGqXZ43PGmX6ejkmygy5bgLflpunmdNCk&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//           "https://api.pcloud.com/getpubthumb?code=XZoTGqXZx6cesnnqGRb0m07Vyi1n80APIvEy&linkpassword=undefined&size=2048x1152&crop=0&type=auto",
//         ],
//       },
//     ],
//   },
// ];
